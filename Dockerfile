FROM php:7.2-fpm

RUN apt-get update -y \
    && apt-get install -y nginx

COPY index.php ./
COPY docker/nginx/nginx.conf /etc/nginx/sites-enabled/default
COPY entrypoint.sh /etc/entrypoint.sh

COPY --chown=www-data:www-data . /var/www

EXPOSE 80
ENTRYPOINT ["sh", "/etc/entrypoint.sh"]